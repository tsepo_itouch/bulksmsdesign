/**
 * Created by tsepo on 2017/03/10.
 */

$(document).ready(function() {

    var $body   = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;
    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });
    $("#pricing .price-item").hover(function () {
        $(this).find(".price").toggle() ;
        $(this).find(".zar").toggle() ;
        $(this).find(".discount").toggle() ;
        $(this).find(".rate").toggle() ;
    });
    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });
});
