/**
 * Created by tsepo on 2017/03/10.
 */

$(document).ready(function() {


    // image resize on scroll on home page
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 50) {
            $('nav div a img.brand').css('width', '35px');

        } else {
            $('nav div a img.brand').css('width', '75px');
        }
    });
    // image resize on scroll on home page
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();


        if($( window ).width() < 320){

            $('img.brand').css('width', '35px');
        }
        else if ($( window ).width() >= 320 && $( window ).width() < 480 ){
            if (scroll > 50 ){
                $('img.brand').css('width', '35px');
            }
            else{
                $('img.brand').css('width', '50px');
            }
        }
        else if ($( window ).width() >= 480 && $( window ).width() < 600 ){
            if (scroll > 50 ){
                $('img.brand').css('width', '35px');
            }
            else{
                $('img.brand').css('width', '65px');
            }
        }
        else if ($( window ).width() >= 600) {
            if (scroll > 50 ){
                $('img.brand').css('width', '35px');
            }
            //$('img.brand').css('width', '75px');
        }
    });

    var $body   = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;
    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });
    $("#pricing .price-item").hover(function () {
       $(this).find(".price").toggle() ;
        $(this).find(".zar").toggle() ;
       $(this).find(".discount").toggle() ;
        $(this).find(".rate").toggle() ;
    });
    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });
});
